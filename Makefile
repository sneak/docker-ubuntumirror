default: build

build:
	docker build -t sneak/ubuntumirror .

up:
	docker-compose --project-name local up -d mirror
