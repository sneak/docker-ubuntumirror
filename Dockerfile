FROM phusion/baseimage:0.11

VOLUME /var/mirror

ENV MIRROR_UPDATE_INTERVAL_SECONDS 43200
ENV UBUNTU_MIRROR_ARCHITECTURES amd64
# set to anything but 'false' to mirror source code as well
ENV UBUNTU_ENABLE_SOURCE_MIRRORING false
ENV UBUNTU_MIRROR_CATEGORIES main,universe,restricted,multiverse,main/debian-installer
ENV UBUNTU_MIRROR_UPSTREAM archive.ubuntu.com
ENV UBUNTU_MIRROR_UPSTREAM_PATH /ubuntu
ENV UBUNTU_MIRROR_PROJECTS bionic,bionic-updates,bionic-security,bionic-backports,focal,focal-updates,focal-security,focal-backports

# The following is the URL populated in /mirrors.txt as used by mirror://
# protocol scheme in sources.list
# the idea is that you can DNS rewrite 'mirrors.ubuntu.com' to this mirror
# and it will return a "list" of mirrors that includes only itself.
# then you can safely use something like the following in your sources.list
# and it will work with max speed on your own dns-rewritten lan and in the
# unmodified datacenter talking to the real mirrors.ubuntu.com:
#
# MURM="main universe restricted multiverse"
# C="$(lsb_release -cs)"
# M="mirror://mirrors.ubuntu.com/mirrors.txt"
# echo "deb $M $C $MURM" > /etc/apt/sources.list.new
# echo "deb $M $C-updates $MURM" >> /etc/apt/sources.list.new
# echo "deb $M $C-backports $MURM" >> /etc/apt/sources.list.new
# echo "deb $M $C-security $MURM" >> /etc/apt/sources.list.new
# mv /etc/apt/sources.list.new /etc/apt/sources.list
ENV THIS_MIRROR_URL http://172.17.0.1/ubuntu/

ADD ./sources.list /etc/apt/sources.list
RUN echo "#!/bin/sh\nexit 101" > /usr/sbin/policy-rc.d
# phusion baseimage says i'm not supposed to do this
# and i don't care:
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y run-one nginx make debmirror xz-utils

ADD ./mirror /opt/mirror
ADD ./mirror.run /etc/service/mirror/run
ADD nginx.conf /etc/nginx/nginx.conf
ADD ./nginx.run /etc/service/nginx/run

RUN chmod +x /etc/service/nginx/run && \
    chmod +x /etc/service/mirror/run

RUN \
    export GNUPGHOME=/etc/debmirror/ubuntu && \
    mkdir -p $GNUPGHOME && \
    chmod go-rwx $GNUPGHOME && \
    gpg --import /usr/share/keyrings/ubuntu-archive-keyring.gpg && \
    cp $GNUPGHOME/pubring.kbx $GNUPGHOME/trustedkeys.kbx


EXPOSE 80
