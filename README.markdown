# sneak/ubuntumirror

Docker image for syncing ubuntu LTS mirrors to reduce network traffic and
speed up build times.

Then even your builds (where you can't specify links to other containers by
hostname) will go fast.

# Levers

## Paths

The mirror stores all the data in a docker volume at path
`/var/mirror/ubuntu`.  Map that somewhere else if you like.

## Environment Variables

* `UBUNTU_MIRROR_ARCHITECTURES`
    * default `amd64`.
    * You may want `i386,amd64`.
* `UBUNTU_MIRROR_CATEGORIES`
    * Default `main,universe,restricted,multiverse`. Will use lots of disk.
    * You may want `main,universe`.
* `UBUNTU_MIRROR_PROJECTS`
    * Default
      `bionic,bionic-updates,bionic-security,bionic-backports,focal,focal-updates,focal-security,focal-backports`.
    * You may want only `focal,focal-updates,focal-security,focal-backports`.
* `UBUNTU_MIRROR_UPSTREAM` - upstream server to mirror from
    * default `archive.ubuntu.com`.
    * You may want `xx.archive.ubuntu.com` (where `xx` is your country
      code).
* `UBUNTU_ENABLE_SOURCE_MIRRORING` - do we want to mirror all the source code too?
    * Default ``
    * You may want `1` if you want to use up even more disk for your mirror
* `MIRROR_UPDATE_INTERVAL_SECONDS` - mirror update interval
    * default `43200` (12h)
    * You may want `10800` (3h)

# Example Startup

```
docker rm -f local-ubuntu-mirror
docker run -d --name local-ubuntu-mirror \
    -v /storage/mirrors/ubuntu:/var/mirror/ubuntu \
    --env MIRROR_UPDATE_INTERVAL_SECONDS=3600 \
    --env UBUNTU_MIRROR_ARCHITECTURES=amd64 \
    --env UBUNTU_MIRROR_UPSTREAM=us.archive.ubuntu.com \
    --env UBUNTU_MIRROR_PROJECTS=focal,focal-updates,focal-security,focal-backports \
    -p 8080:80 \
    --restart unless-stopped \
    sneak/ubuntumirror@sha256:7de7cc8245106bb574029a2dedca5aa218af08d802b989cdc7857fb57f876a02
```

# Example CapRover Definition File

```
{
  "schemaVersion": 2,
  "imageName": "sneak/ubuntumirror@sha256:7de7cc8245106bb574029a2dedca5aa218af08d802b989cdc7857fb57f876a02"
}
```

# Author

sneak &lt;[sneak@sneak.berlin](mailto:sneak@sneak.berlin)&gt;

# License

This software is released into the public domain.

