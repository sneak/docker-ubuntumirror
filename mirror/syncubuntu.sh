#!/bin/bash
set -x

THISDIR="$(cd $(dirname "$BASH_SOURCE") && pwd -P)"
DST="/var/mirror"

echo "$THIS_MIRROR_URL" > $DST/mirrors.txt
chmod a+r $DST/mirrors.txt

mkdir -p "$DST/ubuntu"

export GNUPGHOME="/etc/debmirror/ubuntu"

SOURCEOPT="--nosource"
if [[  "$UBUNTU_ENABLE_SOURCE_MIRRORING" != "false" ]]; then
    SOURCEOPT="--source"
fi

DONE=0

DIDIST="$(
    echo -n "$UBUNTU_MIRROR_PROJECTS" | tr "," "\n" | grep -v "\-" | tr "\n" ","
)"

while [[ $DONE -eq 0 ]]; do
    debmirror \
        -a ${UBUNTU_MIRROR_ARCHITECTURES} \
        -s ${UBUNTU_MIRROR_CATEGORIES} \
        -h ${UBUNTU_MIRROR_UPSTREAM} \
        -d ${UBUNTU_MIRROR_PROJECTS} \
        $SOURCEOPT \
        --di-dist="$DIDIST" \
        --di-arch=arches \
        --ignore-small-errors \
        -r "$UBUNTU_MIRROR_UPSTREAM_PATH" \
        --getcontents \
        --progress \
        --method=http \
        $DST/ubuntu
    if [[ $? -eq 0 ]]; then
        DONE=1
    fi
    chmod -R a+rX $DST
    sleep 1
done
